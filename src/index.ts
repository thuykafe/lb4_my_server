import {BikingAppApplication} from './application';
import {HelloApplication} from './hello.application';
import {ApplicationConfig} from '@loopback/core';

export {BikingAppApplication, HelloApplication};

export async function main(
  options: ApplicationConfig = {},
  pub_options: ApplicationConfig = {},
) {
  const app = new BikingAppApplication(options);
  const hello = new HelloApplication(pub_options);

  await app.boot();
  await app.start();

  await hello.boot();
  await hello.start();

  const url = hello.restServer.url;
  console.log(`Server is running at ${url}`);

  return app;
}
