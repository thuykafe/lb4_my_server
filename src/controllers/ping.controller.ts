// Uncomment these imports to begin using these cool features!

import {inject} from '@loopback/context';
import {get, post, param, requestBody} from '@loopback/rest';
import {authenticate} from '@loopback/authentication';
import {SecurityBindings, UserProfile} from '@loopback/security';

export class PingController {
  constructor() {}

  @get('/ping')
  @authenticate('jwt')
  async getHello(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<string> {
    const result = `Hello user ${currentUserProfile.name} has email ${currentUserProfile.email}`;
    return result;
  }

  @post('/ping')
  postHello(
    @param.query.string('pwResetToken') pwResetToken: string,
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email', 'password'],
            properties: {
              email: {
                type: 'string',
                format: 'email',
              },
              new_password: {
                type: 'string',
                minLength: 8,
              },
            },
          },
        },
      },
    })
    cred: {
      email: string;
      password: string;
    },
  ): string {
    return 'Hello ' + cred.email + ' with token ' + pwResetToken;
  }
}
