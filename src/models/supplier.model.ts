import {Entity, model, property} from '@loopback/repository';

@model()
export class Supplier extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    default: 'Untitled',
  })
  name: string;

  @property({
    type: 'number',
    default: 2,
  })
  rates?: number;

  constructor(data?: Partial<Supplier>) {
    super(data);
  }
}

export interface SupplierRelations {
  // describe navigational properties here
}

export type SupplierWithRelations = Supplier & SupplierRelations;
