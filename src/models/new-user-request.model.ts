import {model, property, Model} from '@loopback/repository';
import {User} from '.';

@model()
export class NewUserRequest extends Model {
  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    required: true,
    jsonSchema: {
      exclude: ['id'],
    },
  })
  user: User;

  constructor(data?: Partial<NewUserRequest>) {
    super(data);
  }
}
export interface NewUserRequestRelations {
  // describe navigational properties here
}

export type NewUserRequestWithRelations = NewUserRequest &
  NewUserRequestRelations;
