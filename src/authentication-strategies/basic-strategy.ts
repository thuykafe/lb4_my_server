import {AuthenticationStrategy} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {UserProfile} from '@loopback/security';
import {Request, HttpErrors} from '@loopback/rest';

import {AuthenticationStrategyBindings, UserServiceBindings} from '../keys';
import {Credentials} from '../repositories/user.repository';
import {AuthenticationUserService} from '../services';

export interface BasicAuthOptions {
  [property: string]: any;
}

export class BasicAuthenticationStrategy implements AuthenticationStrategy {
  name = 'basic';

  constructor(
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: AuthenticationUserService,
    @inject(AuthenticationStrategyBindings.Basic.DEFAULT_OPTIONS)
    public readonly options?: BasicAuthOptions,
  ) {}

  async authenticate(
    request: Request,
    options?: BasicAuthOptions,
  ): Promise<UserProfile | undefined> {
    // override the global set options with the one passed from the caller
    options = options ?? this.options;

    try {
      const credentials = this.extractCredentials(request);
      // `verifyCredentials` throws error accordingly: user doesn't exist OR invalid credentials

      const user = await this.userService.verifyCredentials(credentials);
      const userProfile = this.userService.convertToUserProfile(user);
      return userProfile;
    } catch (e) {
      throw e;
    }
  }

  extractCredentials(request: Request): Credentials {
    if (!request.headers.authorization) {
      throw new HttpErrors.Unauthorized(`Authorization header not found.`);
    }
    // for example : Basic Z2l6bW9AZ21haWwuY29tOnBhc3N3b3Jk
    const authHeaderValue = request.headers.authorization;

    if (!authHeaderValue.startsWith('Basic')) {
      throw new HttpErrors.Unauthorized(
        `Authorization header is not of type 'Basic'.`,
      );
    }

    //split the string into 2 parts. We are interested in the base64 portion
    const parts = authHeaderValue.split(' ');
    if (parts.length !== 2)
      throw new HttpErrors.Unauthorized(
        `Authorization header value has too many parts. It must follow the pattern: 'Basic xxyyzz' where xxyyzz is a base64 string.`,
      );
    const encryptedCredentails = parts[1];

    // decrypt the credentials. Should look like :   'username:password'
    const decryptedCredentails = Buffer.from(
      encryptedCredentails,
      'base64',
    ).toString('utf8');

    //split the string into 2 parts
    const decryptedParts = decryptedCredentails.split(':');

    if (decryptedParts.length !== 2) {
      throw new HttpErrors.Unauthorized(
        `Authorization header 'Basic' value does not contain two parts separated by ':'.`,
      );
    }

    const creds: Credentials = {
      email: decryptedParts[0],
      password: decryptedParts[1],
    };

    return creds;
  }
}
