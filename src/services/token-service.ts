import {TokenService} from '@loopback/authentication';
import {HttpErrors} from '@loopback/rest';
import {inject} from '@loopback/context';
import {UserProfile, securityId} from '@loopback/security';
import {TokenServiceBindings} from '../keys';
import {promisify} from 'util';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export class JWTokenService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN)
    private jwtExpiresIn: string,
    @inject(TokenServiceBindings.TOKEN_SECRET)
    private jwtSecret: string,
  ) {}

  async generateToken(userProfile: UserProfile): Promise<string> {
    if (!userProfile)
      throw new HttpErrors.Unauthorized(
        'Error when generating token: userProfile is null',
      );

    return signAsync(userProfile, this.jwtSecret, {
      expiresIn: this.jwtExpiresIn,
    });
  }

  async verifyToken(token: string): Promise<UserProfile> {
    if (token === '') {
      throw new HttpErrors.BadRequest('Token can not be empty!');
    }
    let userProfile: UserProfile;

    try {
      userProfile = await verifyAsync(token, this.jwtSecret);
      userProfile[securityId] = userProfile.id;
    } catch (error) {
      throw new Error(error);
    }

    return userProfile;
  }
}
